import { useNavigate } from "react-router-dom";

const ExampleComp = () => {
  const navigate = useNavigate();

  return (
    <>
      <h4>COBA VITE !!</h4>
      <button
        onClick={() => {
          navigate("/users");
        }}
      >
        Go To /users
      </button>
    </>
  );
};

export default ExampleComp;
