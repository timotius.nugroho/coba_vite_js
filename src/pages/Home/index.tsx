import { useState } from "react";
import reactLogo from "../../assets/react.svg";
import ExampleComp from "../../components/ExampleComp";

const Home = () => {
  const [count, setCount] = useState(0);

  return (
    <>
      <div>
        <h1>Vite + React</h1>
        <div>
          <button onClick={() => setCount((count) => count + 1)}>
            count is {count}
          </button>
          <p>
            Edit <code>src/App.tsx</code> and save to test HMR
          </p>
        </div>
        <ExampleComp />
      </div>
    </>
  );
};

export default Home;
